clear all
set obs 6
gen x1=1
gen x2=1
gen x3=7
gen y=1
replace x2 = 2 in 2
replace x2 = 3 in 3
replace x2 = 4 in 4
replace x2 = 5 in 5
replace x2 = 6 in 6
replace x3 = 8 in 3
replace x3 = 8 in 4
replace x3 = 9 in 5
replace x3 = 9 in 6
replace y = 0 in 5
replace y = 0 in 1


clear all 
set obs 30

gen x1=1
gen x2=runiform()
gen x3=rnormal()
gen y=rnormal(2)

cd "/home/damiancclarke/computacion/StataPrograms/Plugins/regression"

matrix X = J(30,3,.)
matrix Y = J(30,1,.)

cap program drop regression_input
program regression_input, plugin
plugin call regression_input x1 x2 x3 y, X Y


